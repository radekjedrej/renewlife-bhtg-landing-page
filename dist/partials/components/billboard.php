<?php
    $modifier        = !empty($modifier) ? $modifier : "";
    $alt             = !empty($alt) ? $alt : "";
    $header          = !empty($header) ? $header : "";
    $preheader       = !empty($preheader) ? $preheader : "";
    $background_img  = !empty($background_img) ? $background_img : "";

    
    if( !empty($background_img) ) {
        $resp_img_data   = Utils\compile_responsive_img_attrs($background_img);
        $srcset_compiled = $resp_img_data['srcset'] ?? [];
        $sizes_compiled  = $resp_img_data['sizes'] ?? 'auto';
    }
?>


<?php if( !empty($srcset_compiled) && !empty($sizes_compiled) ) : ?>
    <section class="billboard lazyload <?=$modifier?>" data-bgset="<?= $srcset_compiled; ?>" data-sizes="<?= $sizes_compiled; ?>">
<?php else : ?>
    <section class="billboard <?=$modifier?>">
<?php endif; ?>

    <?php if($header): ?>
    <header class="billboard__inner">
        <h1 class="billboard__heading heading--alpha heading--primary heading--uppercase">
            <?= $header ?>
            <span class="billboard__sub-heading heading--sub-heading heading--bravo heading--white heading--uppercase">
                <?= $preheader ?>
            </span>
        </h1>
    </header>
    <?php endif; ?>
    
</section>
