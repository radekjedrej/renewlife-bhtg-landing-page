<?php
    // 1. Checking if assets manifest file exists
    $assets_manifest = Utils\check_asset_manifest();

    // 2. If the manifest file does exist get path to main.js else use dev version
    $js_main = ( !empty($assets_manifest) ) ? webpackAssets::$jsFiles['main'] : "main.js";

    // 3. Checking if the manifest file does exist get path to inline.js
    $js_inline = ( !empty($assets_manifest) ) ? webpackAssets::$jsFiles['inline'] : "";
?>

<?php if ($js_inline): ?>
    <script type="text/javascript">
        <?php include_once $js_inline; ?>
    </script>
<?php endif; ?>

<script src="<?= $js_main; ?>" charset="utf-8"></script>
