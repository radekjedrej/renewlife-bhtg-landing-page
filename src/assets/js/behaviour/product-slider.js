import Glide from '@glidejs/glide';
// import BadgerAccordion from 'badger-accordion';
const carouselNode = (document.querySelector('.js-glide--product-slider') !== null) ? document.querySelector('.js-glide--product-slider') : null;

if( carouselNode !== null ) {
    // debugger;
    // const accordions = document.querySelectorAll('.js-badger-accordion');
    // const accordionArray = [];

    // accordions.forEach((accordion, i) => {
    //     accordionArray[i] = new BadgerAccordion(accordion, {
    //         openMultiplePanels: true,
    //     });
    // });

    const productSlider = new Glide('.js-glide--product-slider', {
        focusAt: 'center',
        dragThreshold: 80,
        gap: 0,
        // perView: 6,
        swipeThreshold: 50,
        type: 'carousel',
        breakpoints: {
            600: {
                // gap: 0,
                perView: 2
            },
            900: {
                perView: 3
            },
            1200: {
                perView: 4
            },
            10000: {
                perView: 5
            }
        },
    });

    productSlider.on(['build.after', 'run'], function() {
        // When the slider loads, and each time the slide changes, update the content panel below the slider.
        updatePanel();
    });

    productSlider.mount();

    // updatePanel: Handles visibility and classes for the content linked to each product in the slider.
    function updatePanel() {
        // First, collate all of the content panels and hide them.
        const contentPanels = document.querySelectorAll('.product-slider__panel-content');

        contentPanels.forEach(
            (panel) => panel.style.display = 'none'
        );

        // Grab the current slide index from the Glide object. Find the linked content panel, and display it.
        // Product IDs are set in `data/product-data.php`.
        const currentIndex = productSlider.index;
        const currentPanel = document.querySelector(`[data-index="${currentIndex}"]`);
        currentPanel.style.display = 'block';

        // Update the classes on the content panels, based on the currently active panel.
        updatePanelClasses( currentPanel );

        // Recalculate panel heights as the accordion was previously hidden
        // accordionArray[currentIndex].calculateAllPanelsHeight();
        // accordionArray[currentIndex].init();

        // Check that the Google Analytics script has loaded, then send a View event for the current slide.
        if( typeof ga === 'function' ) {
            const slideView = {
                eventCategory: 'Slider',
                eventAction: 'view',
                eventLabel: currentIndex.toString(),
            };

            ga('send', 'event', slideView);
        }
    }

    // Because we're registering the accordion every time a new content panel is loaded, the first panel has some issues with styling.
    // To get around this, we recalculate the height of the accordion panels for the first content panel when the accordion panel headers are clicked.

    // const firstInputs = document.querySelectorAll('.js-product-accordion--0 .js-badger-accordion-header');

    // firstInputs.forEach((input) => {
    //     input.addEventListener('click', () => {
    //         accordionArray[0].calculateAllPanelsHeight();
    //     });
    // });

    const slides = document.querySelectorAll('.js-glide__slide');

    slides.forEach((slide) => {
        slide.addEventListener('click', () => {
            productSlider.go(`=${slide.dataset.slideId}`);
        });
    });

    function updatePanelClasses( currentPanel ) {
        // Select the content panel container and remove any modifier classes.
        const panelContainer = document.querySelector('.separate-content');
        panelContainer.className = 'separate-content band';

        // Add a class based on the product's theme - themes are set in `product-data.php`.
        panelContainer.classList.add(`separate-content--${ currentPanel.dataset.theme }`);

        // Select the marker triangle SVG and remove any modifier classes.
        const sliderMarker = ( document.querySelector('.product-slider__marker--svg') ? document.querySelector('.product-slider__marker--svg') : null);

        if(sliderMarker !== null) {
            sliderMarker.setAttribute('class', 'product-slider__marker--svg');

            // Add a class based on the product's theme.
            sliderMarker.classList.add(`product-slider__marker--svg--${ currentPanel.dataset.theme }`);
        }
    }
}
