<?php 

    include_once 'data/product-data.php';
    $allProducts = $allProducts ?? '';
    if( !empty($allProducts) ) :

?>

<section>

    <header class="container text-center">
        <h2 class="heading--bravo heading--section heading--primary">Learn about our supplements</h2>
    </header>
    
    <div class="container container--expanded">
        <?php
            echo Utils\nb_load_template_part('partials/components/slider/glide-slider', array(
                'allProducts' => $allProducts,
            ));
        ?>
    </div>

    <svg class="product-slider__marker" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 30" width="56"><style>.product-slider__marker--svg{fill:#003b71}</style><path class="product-slider__marker--svg" d="M2.2 31L28 1.5 53.8 31z"/></svg>
            
    <div class="separate-content band">
        <div class="container container--reduced">
 
            <?php
                foreach ($allProducts as $product_id => $product):

                    echo Utils\nb_load_template_part('partials/components/slider/panel-content', array(
                        'product'    => $product,
                        'product_id' => $product_id,
                    ));

                endforeach;
            ?>

        </div>
    </div>

</section>

<?php
    endif;