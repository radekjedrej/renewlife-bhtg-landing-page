<?php
    $gtag_id = getenv('GTAG_TRACKING_ID') ?? "";
    $ga_id = getenv('GA_TRACKING_ID') ?? "";
?>

<?php // if ( !empty($gtag_id) && !empty( Utils\get_node_env() ) && Utils\get_node_env() === 'production' ): ?>
<!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?= $gtag_id ?>');
        window['GoogleAnalyticsObject'] = 'ga';
        window['ga'] = window['ga'] || function() {
            (window['ga'].q = window['ga'].q || []).push(arguments)
        };
        ga('create', '<?= $ga_id ?>');
    </script>
<?php // endif; ?>
