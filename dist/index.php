<?php include_once 'partials/core/boot.php'; ?>


<!DOCTYPE html>
<html lang="en-GB" class="no-js">
    <head>
        <title>Being Human Takes Guts</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <meta name="description" content="Being human takes guts. Make sure yours are up for it. It’s no secret that a well-balanced gut leads to more energy, better digestion and improved immune health.">

        <?php include_once 'partials/core/head.php'; ?>
    </head>

    <body>
        <?= Utils\nb_load_template_part('partials/core/google-tag-manager-body'); ?>
        <?= Utils\nb_load_template_part('partials/components/loading-spinner'); ?>

        <?= Utils\nb_load_template_part('partials/core/header', [
            'heading' => 'Ready Frontend Boiletplate'
        ]); ?>


        <main id="main-content" class="main">
            <?= Utils\nb_load_template_part('partials/components/billboard', array(
                'modifier'       => 'billboard--sunrays',
                'header'         => 'Being human <br>takes guts.',
                'preheader'      => 'Make sure yours <br>are up for it.',
            )); ?>

            <?= Utils\nb_load_template_part('partials/components/slider/wrapper'); ?>
            
            <?= Utils\nb_load_template_part('partials/components/icon-list'); ?>
            
            <?= Utils\nb_load_template_part('partials/components/video-section'); ?>

            <?= Utils\nb_load_template_part('partials/components/primary-feature/wrapper'); ?>

            <?= Utils\nb_load_template_part('partials/components/press-logos'); ?>
        </main>

        <?= Utils\nb_load_template_part('partials/core/footer'); ?>    

        <?php include_once 'partials/core/javascript-assets.php'; ?>
    </body>
</html>
