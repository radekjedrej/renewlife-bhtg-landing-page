# Renew Life Being Human Takes Guts

## Things to note
 - There was an accordion being used inside the panel of content for each carousel slide. This was removed due to an incositant bug in iOS 12 Safari. The JS was comented out inside `product-slider.js` and markup was removed. See `1.1.3` if you need the markup


## Learnings

### How to create a list of icons while keep them all proportionally sizes and using SVGs

 1. The images all need to be artworked to be the size the designer requires. They will need to be sizes relative to each other so that in Ai/Sketch the designer has them looking how they will want on the larger screens.

 2. SVGs will need a `viewBox` and `height` attribute. This is essential for keeping the logos all proportional to each other

 3. Use `flex` to created a `flex-wrap: wrap`ed list. Add spacing to the right and below each list item. Apply negative margin of the same amount to the list element. You will need to add `overflow: hidden;` to the parent item so that you don't get any weird issues on small device.

 ```
 .logo-grid { overflow: hidden; }

 .logo-grid__list {
    display: flex;
    flex-wrap: wrap;
    margin-right: -40px;
    margin-bottom: -20px;
    // your styles to suit
 }

 .logo-grid__list-item {
    display: flex;
    margin-right: 40px;
    margin-bottom: 20px;
 }
 ```

 4. For controlling your columns set a width on each list item. I found it most reliable x-browser to do this using css `calc` to minus the spacing around the item from the width. Eg for a 2 column layout;
 
```
// width: calc(50% - #{spacing either side of the parent + spacing right of item})
width: calc(50% - 60px);
```

5. Each SVG needs to have a `max-width` with an absolute value. This is to keep all the images their relative sizes when inside a column layout.

```
max-width: 160px;
```