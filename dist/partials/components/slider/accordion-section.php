<?php
    $heading = $heading ?? '';
    $content = $content ?? '';

    if( !empty($heading) && !empty($content) ):
?>

    <dt class="landmark--quarter">
        <h4 class="product-accordion__header heading--delta heading--uppercase heading--reset"><?= $heading ?></h4>
    </dt>

    <dd class="product-accordion__content landmark--quarter">
        <?= $content ?>
    </dd>

<?php
        endif;