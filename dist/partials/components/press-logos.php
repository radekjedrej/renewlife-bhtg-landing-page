<?php 
    include_once 'data/press-logos.php';

    $data = $press_logos ?? '';
?>

<?php if( !empty($data) ) : ?>
    <section class="press-logos">
        <h2 class="heading--delta heading--uppercase heading--center landmark">Featured in</h2>

        <div class="container">
            <ul class="press-logos__list">


                <?php foreach($data as $logo) : ?>
                    <?php
                        $raster   = $logo['raster'] || false;
                        $svg_path = $logo['svg_path'] ?? '';
                        $img      = $logo['img'] ?? '';
                    ?>

                        <?php if( !empty($svg_path) || !empty($img) ) : ?>
                        
                            <li class="press-logos__list-item">
                                <?php if( $raster ) : ?>
                                    <?= Utils\nb_load_template_part('partials/components/shared/responsive-inline-img', [
                                        'img' => $img
                                    ]); ?>
                                <?php else : ?>
                                    <?= file_get_contents($svg_path); ?>
                                <?php endif; ?>
                            </li>

                        <?php endif; ?>    

                <?php endforeach; ?>


            </ul>
        </div>
    </section>
<?php endif; ?>
