<?php
    $img_setup =  [
        'srcset'    => [320, 400, 640, 800, 1280, 1600],
        'sizes' => [
            [
                'bp'   => '320px',
                'size' => '100vw'
            ],
            [
                'bp'   => '640px',
                'size' => '100vw'
            ],
            [
                'bp'   => '855px',
                'size' => '95vw'
            ],
            [
                'max_width' => false,
                'bp'        => '1280px',
                'size'      => '394px'
            ]
        ]
    ];

    $large_img_setup =  [
        'srcset'    => [320, 400, 640, 800, 1280, 1600, 1920],
        'sizes' => [
            [
                'size' => '100vw'
            ]
        ]
    ];

    $articles_data = [
        [
            'id'      => '1',    
            'img'     => array_merge(
                $img_setup,
                ['base_path' => '/assets/img/articles/1/resolutions-']
            ),
            'large_img' => array_merge(
                $large_img_setup,
                ['base_path' => '/assets/img/articles/1/resolutions-']
            ), 
            'heading' => '5 Simple Ways to Reclaim Your Resolutions',
            'excerpt' => 'For a lot of people, the beginning of February means coming to terms with failed New Year’s resolutions. I think you’d agree that change...',
            'cta'     => [
                'link' => '/five-simple-ways',
                'text' => 'Read more'
            ]
        ],
        [
            'id'      => '2',    
            'img'     => array_merge(
                $img_setup,
                ['base_path' => '/assets/img/articles/2/gooddigestion-']
            ),
            'large_img' => array_merge(
                $large_img_setup,
                ['base_path' => '/assets/img/articles/2/gooddigestion-']
            ), 
            'heading' => 'Good Digestive Health Begins with live bacteria',
            'excerpt' => 'All too often digestion can seem like an automatic process, but nowadays scientists are discovering that we need to have a different...',
            'cta'     => [
                'link' => '/good-digestive-health',
                'text' => 'Read more'
            ]
        ],
        [
            'id'       => '3',  
            'modifier' => 'primary-feature--bad-crop',
            'img'      => array_merge(
                $img_setup,
                ['base_path' => '/assets/img/articles/3/naturalbody-']
            ),
            'large_img' => array_merge(
                $large_img_setup,
                ['base_path' => '/assets/img/articles/3/naturalbody-']
            ), 
            'heading' => 'Natural Body Cleansing: What are the Benefits and Where to Begin?',
            'excerpt' => 'Each day we are confronted by harmful toxins in the air we breathe, the food and water we consume, and even in the products we use every...',
            'cta'     => [
                'link' => '/natural-body-cleansing',
                'text' => 'Read more'
            ]
        ]
    ]
?>