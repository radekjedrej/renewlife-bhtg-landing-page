<?php
  $modifier  = !empty($modifier) ? $modifier : "";
?>

<div class="long-form-copy <?=$modifier?>">
    <header>
        <h1 class="heading--alpha heading--primary heading--uppercase">5 SIMPLE WAYS TO RECLAIM YOUR RESOLUTIONS</h1>
        <h2 class="heading--bravo text--bold heading--primary landmark text--uppercase heading--sub-heading">WITH EFFECTIVE, PRACTICAL, AND EASY-TO-IMPLEMENT STRATEGIES</h2>
    </header>

    <p class="landmark">For a lot of people, the beginning of February means coming to terms with failed New Year’s resolutions. I think you’d agree that change doesn’t happen overnight. We can’t exercise twice a month, then take up Zumba, weightlifting and running come the New Year.</p>

    <h3 class="heading--delta text--uppercase landmark--quarter">If you’ve switched up your sugary cereal for a wholesome delicious breakfast, congrats! You’re on the right track!</h3>

    <p class="landmark">But if you’ve attempted to replace your morning Peppermint Mocha Latte (that contains a whopping 65 grams of sugar) with a green vegetable smoothie that you can barely get down, are you really surprised that you’re back to sipping lattes?</p>

    <h3 class="heading--delta text--uppercase landmark--quarter">Change takes time</h3>

    <p class="landmark">And, if you really want to create a new healthy routine and get your taste buds embracing new flavours, patience is key.</p>

    <blockquote class="quotation-mark quotation-mark--primary landmark">
        <p class="border-left--primary text--large text--italic">Some of the greatest motivational gurus of all time remind us that we cannot change daily habits overnight. If our habits take years to build, they will take more than a few days to replace.</p>
    </blockquote>

    <p class="landmark--quarter">Quite frankly, it is consistent small steps that will lead to transformation.</p>
    
    <p class="landmark">Over the past decade, I’ve helped my patients set goals, I’ve supported them through their challenges, and I’ve been there to celebrate their achievements. Let’s just say, I know a little bit about helping people create long-lasting, maintainable lifestyle changes.</p>

    <h3 class="heading--delta text--uppercase landmark-quarter">So if you are excited about a new approach, I’ve gathered up my best tips that have helped thousands of my patients succeed.</h3>

    <p class="landmark--quarter">Whether you are setting goals in January or August, these strategies are effective, practical, and easy-to-implement.</p>

    <p class="landmark--double">Let's dive in.</p>

    <h2 class="heading--delta heading--primary text--uppercase landmark long-form-copy__separator">5 Tips for a Long-Lasting and Maintainable Healthy Lifestyle</h2>

    <h3 class="heading--delta text--uppercase landmark--quarter">#1 – Stop Removing, Start Adding</h3>

    <p class="landmark">One of the prime mistakes we make in trying to lead a healthier lifestyle is ‘take away.’ Perhaps you can relate. Ever catch yourself saying, “I gave up coffee, I cut out carbs, I stopped eating desserts!” Now I know we’re all trying to be healthier, but can you see how this sets us up for failure right from the beginning?</p>

    <p class="landmark--quarter heading--delta text--uppercase long-form-copy__title"><strong>Rather than eliminating all the things you once loved…</strong></p>

    <p class="landmark--quarter">Try keeping them in and simply adding more of the good stuff…</p>
    <p class="landmark--quarter">More herbal teas, more root vegetables, and more fruit is a better way to slowly and gently bring about change. Over time, you may even notice your taste buds craving the nutritiously-dense foods.</p>
    <p class="landmark">This non-limiting attitude changes everything!</p>

    <h3 class="heading--delta text--uppercase landmark--quarter">#2 – Keep Doing What You’re Doing</h3>

    <p class="landmark">There’s no need to stop socialising or attending family dinners. I can’t tell you how many times I’ve heard this … “I haven’t seen my friends in months because I don’t want to be tempted.” Imagine being “uber healthy” but not being able to go out and enjoy a nice dinner?</p>

    <p class="heading--delta text--uppercase long-form-copy__title landmark--quarter"><strong>Just because you’re trying to be healthier, doesn’t mean you shouldn’t go out and have a good time.</strong></p>

    <p>One day you may be digging in at an all-you-can-eat buffet, and the next day, you may be cooking up an organic storm in your kitchen.</p>

    <p class="landmark">When we are making changes to our diet and lifestyle, we need to weave it in with our current lifestyle. By looking at ‘healthy living’ as a separate ‘way of living’, we are sadly creating a dividing barrier in our day-to-day lives.</p>

    <h3 class="heading--delta text--uppercase landmark--quarter">#3 – Don’t Be Hard On Yourself</h3>

    <p class="landmark">I can’t say this enough. Being kind is truly one of the best things you can do – for your well-being, your happiness, and your health.</p>

    <p class="landmark--quarter">If you eat a brownie (and it’s not part of your ‘healthy lifestyle’ plan), don’t give up and throw in the towel! Even if you eat the whole tray, don’t sweat it! Leading a healthier lifestyle is a journey and it’s not always going to be perfect.</p>
    <p class="landmark--quarter">Persistence is far more important than perfection – so get back on track and strive for progress, not perfection. Say buh-bye to the all-or-nothing approach because it’s not doing you any good.</p>
    <p class="landmark">Remember to always be gentle with yourself. You’re Doing The Best You Can (Pssst: I suggest writing this down on a sticky note and reading it every single day)!</p>

    <h3 class="landmark--quarter heading--delta text--uppercase">#4 – If It Doesn’t Make You Happy, Try Something Else</h3>

    <p class="landmark">Unless you’re stranded on a desert island, you’ve got choices. Lots of them! As you embark on a healthier journey this year, choose what makes you happy.</p>

    <p class="heading--delta text--uppercase long-form-copy__title"><strong>If you don’t like working out at the gym, you can dance to your favourite jams at home.</strong></p>

    <p class="landmark--quarter">If you don’t like seaweed snacks, why not make your own tasty Energy Balls? And, if you don’t like fish, the good news is you can get your omega-3 fatty acids from fish oils.</p>

    <p class="landmark">Finally, listen to your body. If you’re craving a warm porridge over a cold green juice, there may be an underlying reason why. Rather than force yourself to chug a green juice, why not dig deeper into those mysterious food cravings? Your body may be trying to communicate with you and it’s best not to ignore the messages.</p>

    <h3 class="landmark--quarter heading--delta text--uppercase">#5 – Celebrate Your Baby Steps</h3>

    <p class="landmark--quarter">Sounds like a no brainer, right?</p>

    <p class="landmark--quarter">But all too often, we forget to pause and celebrate how far we’ve come.</p>

    <p class="landmark--quarter">Though your progress may be slower than you’d like, every now and then, pinch yourself, and be grateful for the journey – all of the successes, the challenges, and even the let-downs have brought you one step closer to your goals.</p>

    <p class="landmark--double">Happy Celebrating!</p>
</div>