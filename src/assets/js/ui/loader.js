const copy = document.querySelector('.js-loading-spinner-copy');
const loader = document.querySelector('.am-spinner');

// The load event is fired when a resource and its dependent resources have finished loading.
window.addEventListener('load', function() {
    loader.classList.add('hidden'); // class "loader hidden"
    if(loader.classList.contains('hidden')) { // check if loader has class hidden
        copy.innerHTML = 'Content has loaded.'; // change message to content has loaded
    }
});
