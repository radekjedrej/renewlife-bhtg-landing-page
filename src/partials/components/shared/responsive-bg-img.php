<?php 
    $modifier      = $img['modifier'] ?? '';
    $resp_img_data = Utils\compile_responsive_img_attrs($img);

    $srcset_compiled = $resp_img_data['srcset'] ?? [];
    $sizes_compiled  = $resp_img_data['sizes'] ?? [];
?>

<?php if( !empty($srcset_compiled) && !empty($sizes_compiled) ) : ?>
    <div
        class="<?= $modifier; ?> lazyload"
        data-bgset="<?= $srcset_compiled; ?>"
        data-sizes="<?= $sizes_compiled; ?>"
    >
    </div>
<?php endif; ?>    