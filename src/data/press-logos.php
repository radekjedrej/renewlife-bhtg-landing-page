<?php
    // $svg_base_path    = './assets/img/svg/press-logos/';
    $svg_base_path    = Utils\get_img_dir('svg/press-logos/');
    $raster_base_path = './assets/img/press-logos-raster/';

    $press_logos = [
        [
            'svg_path' => $svg_base_path . 'vogue.svg'
        ],
        [
            'raster'         => true,
            'img'            => [
                'base_path'      => $raster_base_path . 'balance-',
                'srcset'         => [151, 300],
                'file_extension' => '.png'
            ]
        ],
        [
            'svg_path' => $svg_base_path . 'elle.svg'
        ],
        [
            'svg_path' => $svg_base_path . 'evening-standard.svg'
        ],
        [
            'raster'         => true,
            'img'            => [
                'base_path'      => $raster_base_path . 'get-the-gloss-',
                'srcset'         => [171, 341],
                'file_extension' => '.png'
            ]
        ],
        [
            'svg_path' => $svg_base_path . 'health-and-fitness.svg'
        ],
        [
            'raster'         => true,
            'img'            => [
                'base_path'      => $raster_base_path . 'hip-and-healthy-',
                'srcset'         => [279, 558],
                'file_extension' => '.png'
            ]
        ],
        [
            'svg_path' => $svg_base_path . 'mail-online.svg'
        ],
        [
            'svg_path' => $svg_base_path . 'the-observer.svg'
        ],
        [
            'raster'         => true,
            'img'            => [
                'base_path'      => $raster_base_path . 'women-',
                'srcset'         => [127, 254],
                'file_extension' => '.png'
            ]
        ],
        [
            'svg_path' => $svg_base_path . 'womens-health.svg'
        ],
        [
            'svg_path' => $svg_base_path . 'yahoo.svg'
        ]
    ];
?>