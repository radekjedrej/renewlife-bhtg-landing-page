// POLYFILL
//
// Import all polyfill files here. They can come
import './polyfill/promise';
// import './polyfill/array-from';



// VENDOR
//
// Import all vendor files here. They can come
// from `node_modules` or `/vendor` directory.



// UI
//
// Importing all UI modules here.
import './vendor/lazysizes-bgset';
import './vendor/lazysizes-responsive-img-polyfill';
import 'lazysizes';




// BEHAVIOUR
//
// Importing all behaviours modules here.
import './behaviour/font-loading';
import './behaviour/product-slider';
import './behaviour/a11y-card';
import './behaviour/ga-events';
// import './behaviour/site-loader';
// import './behaviour/iframe-a11y-video-player';
