<?php
  $modifier  = !empty($modifier) ? $modifier : "";
?>

<div class="long-form-copy <?=$modifier?>">
    <header>
        <h1 class="heading--alpha heading--primary landmark text--uppercase">
            Good Digestive Health Begins with Live bacteria
        </h1>
    </header>

    <p class="landmark">All too often digestion can seem like an automatic process, but nowadays scientists are discovering that we need to have a different mindset. Specifically, we need to be mindful of the fact that a balanced gut is the foundation for optimal health—and one of the ways we can do that is by understanding the benefits of live bacteria.</p>

    <p class="landmark">When there is an ideal balance of beneficial (and neutral) to harmful bacteria in the gut, the stage is set for optimal digestion. However, several factors such as poor diet, stress, age, and the use of certain medications can deplete our numbers of good bacteria and lead to occasional digestive upsets such as bloating and constipation. Live bacteria are the beneficial or “friendly” bacteria that help balance the gut and promote good digestive health, which is why many experts recommend taking a daily high-potency live bacteria supplement.</p>

    <p class="landmark--double">By working to maintain a balanced intestinal environment, live bacteria enhance digestion and help prevent occasional digestive upsets. They also help to ferment soluble fiber in the gut, which in turn produces a short-chain fatty acid called butyric acid that helps to nourish healthy colon cells and promote bowel regularity. In addition, live bacteria help your body digest valuable nutrients from food and play a key role in manufacturing needed vitamins, including vitamin B12 and vitamin K.</p>
</div>