<?php 
    $bh_id = getenv('BH_PROJECT_KEY') ?? "";
?>

<?php if ( !empty($bh_id) && !empty( Utils\get_node_env() ) && Utils\get_node_env() === 'development' ): ?>
    <script type='text/javascript'>
    (function (d, t) {
    var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
    bh.type = 'text/javascript';
    bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=<?= $bh_id; ?>';
    s.parentNode.insertBefore(bh, s);
    })(document, 'script');
    </script>
<?php endif; ?>