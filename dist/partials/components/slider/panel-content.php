<?php
    $product          = $product ?? '';
    $product_id       = $product_id ?? '';
    $product['theme'] = $product['theme'] ?? '';
    $about_copy       = $product['about'] ?? '';

    if( !empty($product['name']) && !empty($product['about']) && !empty($product['features']) && !empty($product['ingredients']) && isset($product_id) ):
?>

    <div class="product-slider__panel-content" data-index="<?= $product_id ?>" data-theme="<?= $product['theme'] ?>" itemscope itemtype="http://schema.org/Product">

    <div class="panel-content__header text-center landmark">
        <h3 class="heading--charlie heading--section text-center landmark" itemtype="http://schema.org/name">
            <?= $product['name'] ?> <br>
            <span class="heading--regular"><?= $product['quantity'] ?></span>
        </h3>

        <?php if( !empty($product['url']) ): ?>
            <a class="btn product-slider__button--buy-now" href="<?= $product['url'] ?>" data-for-product="<?= $product['name'] ?>">Buy Now</a>
        <?php endif; ?>
    </div>

    <?php
        if( !empty($product['highlights']) ){
            echo Utils\nb_load_template_part('partials/components/slider/product-highlights', array(
                'product_highlights' => $product['highlights'],
            ));
        }
    ?>


    <?php if( !empty($about_copy) ) : ?>
        <div class="text--white landmark">
            <?= $about_copy; ?>
        </div>
    <?php endif; ?>


    <?php
        echo Utils\nb_load_template_part('partials/components/slider/accordion-container', array(
            'product'    => $product,
            'product_id' => $product_id,
        ));
    ?>

    <?= Utils\nb_load_template_part('partials/components/slider/where-to-buy'); ?>

    <p class="panel-content__disclaimer text--smallest">Food supplements should not be used as a substitute for a varied and balanced diet and healthy lifestyle.</p>

    </div>

<?php endif;