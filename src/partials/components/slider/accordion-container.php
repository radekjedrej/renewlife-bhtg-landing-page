<?php
    $product = $product ?? '';
    $product_id = $product_id ?? '';
    if( !empty($product['about']) && !empty($product['features']) && !empty($product['ingredients']) && isset($product_id) ):
?>

<dl class="landmark" itemtype="http://schema.org/description">

    <?php
        echo Utils\nb_load_template_part('partials/components/slider/accordion-section', array(
            'heading' => 'Features',
            'content' => $product['features'],
        ));
        echo Utils\nb_load_template_part('partials/components/slider/accordion-section', array(
            'heading' => 'Ingredients',
            'content' => $product['ingredients'],
        ));
    ?>

</dl>

<?php 
    endif;