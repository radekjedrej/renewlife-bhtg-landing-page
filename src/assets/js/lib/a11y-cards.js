class A11yCard {
    constructor(el, options) {
        this.el = el;

        const defaults = {
            cardClass: '.js-a11y-cards-item',
            cardLinkClass: '.js-a11y-cards-item-link'
        };

        // Options
        this.settings = Object.assign({}, defaults, options);

        // Selecting all cards and creating an array from Node list
        this.cards = Array.from(this.el.querySelectorAll( this.settings.cardClass ));

        this.init();
    }

    init() {
        this.addEventListeners();
    }

    addEventListeners() {
        this.cards.forEach((card) => {
            // @TODO: Make the follow selections more efficnet
            let down = card.querySelector( this.settings.cardLinkClass );
            let up = card.querySelector( this.settings.cardLinkClass );
            const link = card.querySelector( this.settings.cardLinkClass );

            card.addEventListener('click', (e) => {
                if(link !== e.target) {
                    link.click();
                }
            });

            card.onmousedown = () => down = +new Date();

            card.onmouseup = () => {
                up = +new Date();
                if ((up - down) < 200) {
                    link.click();
                }
            };
        });
    }
}

export default A11yCard;
