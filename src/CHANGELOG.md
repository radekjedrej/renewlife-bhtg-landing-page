
# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

For more information about keeping good change logs please refer to [keep a changelog](https://github.com/olivierlacan/keep-a-changelog).

### ADDED
- CHANGELOG.MD

### CHANGED
- How footer is checking if social networks have a link

### FIXED
- A bug that was super annoying


## [1.2.1] - 2019-3-14
### UPDATED
 - Tweaking content sizes for "accordion" copy


## [1.2.0] - 2019-3-14
### FIXED
 - BH#83: Reducing billboard size on single article
 - BH#84: Correcting spacing on natural health article
 - BH#85: Adjusting mobile sizing of banner logos
 - BH#86: Sorting mobile positioning related articles
 - Ensuring single blog billboard uses correct imgs

### CHANGE
 - Refactoring to remove accordion in panel. This is due to bug we can't reliably replicate with the accordion that was inside the carousel panel content.

### ADDED
 - New 1920px images for single article


## [1.1.3] - 2019-3-13
### FIXED
 - iOS 12 safari bug with accordion... Maybe this time?


## [1.1.2] - 2019-3-13
### FIXED
 - iOS 12 safari bug with accordion
 - Adjusting spacing at the bottom of the press section
 - Fixing overflow with negative `margin-right` on the press list


## [1.1.1] - 2019-3-13
### UPDATED
 - Made press logos accessible
 - Tweaking padding on press section
 - Sizes of Vouge logo
 - Sorting out sizing on mobile for images


## [1.1.0] - 2019-3-13
### ADDED
 - New section for press logos on the homepage


## [1.0.13] - 2019-3-12
### FIXED
 - Rearranged background rules for sun rays to ensure Safari compatibility


## [1.0.12] - 2019-3-12
### FIXED
 - Google Analytics - convert slide index to string for eventLabel


## [1.0.11] - 2019-3-12
### FIXED
 - Google Analytics - using slide index as label rather than value


## [1.0.10] - 2019-3-11
### ADDED
 - Google Analytics events for:
    - Slider views
    - Video plays
    - Buy Now button presses

### CHANGED
 - Analytics & Tag Manager structure
 - The .env file now requires values for both `GA_TRACKING_ID` and `GTAG_TRACKING_ID`
 - Product highlight SVGs are now accessible, with `role="img"` and title tags.


## [1.0.9] - 2019-3-8
### CHANGED
 - `heading--charlie` mobile type size


## [1.0.8] - 2019-3-8
### CHANGED
 - Updating 50billion product images


## [1.0.7] - 2019-3-8
### ADDED
 - Opacity transition on page loading.


## [1.0.6] - 2019-3-8
### FIXED
 - Single blog posts now display the Renew Life logo correctly. This was due to a ID conflict between the bio logo and the loading spinner.
 - Fix issue regarding hidden social icons on bio pages.


## [1.0.5] - 2019-3-8
### CHANGED
 - Where to Buy SVGs updated with new 'Selected independent health stores' text
 - SVGs aria-hidden and described with visually-hidden text
 - Add award badges to relevant product images
 - Update icon list SVGs with new vertical sizing
 - Update lazysizes bgset ployfill


## [1.0.4] - 2019-3-7
### CHANGED
 - Carousel now has lazyloaded responsive images
 - General copy changes after client feedback

### ADDED
 - Meta desciption


## [1.0.3] - 2019-2-6
### CHANGED
 - Fixed SVG issues on blog single page in bio & profie component.


## [1.0.2] - 2019-2-6
### CHANGED
 - Fixed header logo


## [1.0.1] - 2019-2-6
### CHANGED
 - Optimsied images


## [1.0.0] - 2019-2-6
### CHANGED
 - Inital release