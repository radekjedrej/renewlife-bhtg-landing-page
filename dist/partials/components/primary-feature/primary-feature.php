<?php 
    $modifier = $data['modifier'] ?? '';
    $id       = $data['id'] ?? '';
    $img      = $data['img'] ?? [];
    $heading  = $data['heading'] ?? '';
    $excerpt  = $data['excerpt'] ?? '';
    $cta_link = $data['cta']['link'] ?? '';
    $cta_text = $data['cta']['text'] ?? 'Read more';

    // "pushing" modifier on to $img array
    $img['modifier'] = 'primary-feature__bg-img'
?>

<?php if( !empty($id) && !empty($heading) && !empty($cta_link) ) : ?>
    <article class="primary-feature js-a11y-cards-item <?= $modifier; ?>">
        <?php if( !empty($img) ) : ?>
            <?= Utils\nb_load_template_part('partials/components/shared/responsive-bg-img', [
                'img' => $img
            ]); ?>
        <?php endif; ?>
        <?php 
            // <img
                // src="/assets/img/articles/1/gooddigestion-320.jpg"
                // srcset="/assets/img/articles/1/gooddigestion-320.jpg 320w,
                //     /assets/img/articles/1/gooddigestion-400.jpg 400w,
                //     /assets/img/articles/1/gooddigestion-640.jpg 640w,
                //     /assets/img/articles/1/gooddigestion-800.jpg 800w,
                //     /assets/img/articles/1/gooddigestion-1280.jpg 1280w,
                //     /assets/img/articles/1/gooddigestion-1600.jpg 1600w"
                // sizes="(max-width: 320px) 100vw,
                //     (max-width: 640px) 100vw,
                //     (max-width: 855px) 95vw,
                //     (min-width: 1280px) 394vw"
                // alt=" "
            // >
        ?>

        <div class="primary-feature__inner">
            <h3 class="primary-feature__heading heading--section">
                <a href="<?= $cta_link; ?>" class="primary-feature__link js-a11y-cards-item-link" aria-describedby="primary-feature-<?= $id; ?>">
                    <?= $heading; ?>
                </a>    
            </h3>

            <?php if( !empty($excerpt) ) : ?>
                <p class="primary-feature__copy">
                    <?= $excerpt; ?>
                </p>
            <?php endif; ?>

            <?php if( !empty($cta_text) ) : ?>
                <span id="primary-feature-<?= $id; ?>" class="primary-feature__btn btn btn--small" aria-hidden="true">
                    <?= $cta_text; ?>
                </span>
            <?php endif; ?>
        </div>
    </article>
<?php endif; ?>