<?php
  $modifier            = !empty($modifier) ? $modifier : "";
  $profile_img         = !empty($profile_img) ? $profile_img : "";
  $profile_alt         = !empty($profile_alt) ? $profile_alt : " ";
  $profile_title       = !empty($profile_title) ? $profile_title : "";
  $profile_description = !empty($profile_description) ? $profile_description : "";
?>


<?php if($profile_title): ?>

    <div class="profile--wrapper <?=$modifier?>">
        <?php if($profile_img): ?>
            <img alt="<?=$profile_alt?>" class="profile__img" src="<?=$profile_img?>">
        <?php else:  ?>

        <?= file_get_contents( Utils\get_img_dir("svg/renew-life-mark-logo-circle.svg") ); ?>

        <?php endif; ?>
        
        <div class="profile__content">
            <h2 class="profile__title heading--uppercase"><?=$profile_title?></h2>
            <?php if($profile_description): ?>
                <p class="profile__description"><?=$profile_description?></p>
            <?php endif; ?>
        </div>
    </div>

<?php endif; ?>