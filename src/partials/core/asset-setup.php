<?php
    // 1. Loading composer packages
    require_once Utils\get_project_dir('/vendor/autoload.php');

    // 2. Checking for asset manifest file
    $assets_manifest = Utils\check_asset_manifest();

    // 3. Including the file if it exists
    if( $assets_manifest ) {
        include_once Utils\get_project_dir('assets-manifest.php');
    }

    // 4. Setting up being able to get .env values using - https://github.com/vlucas/phpdotenv
    $dotenv = Dotenv\Dotenv::create(Utils\get_project_dir(), '.env');
    $dotenv->load();
    // To get a value use
    // getenv('YOUR-VALUES-KEY');
?>
