import A11yCards from '../lib/a11y-cards';

const container = (document.querySelector('.js-a11y-cards') !== null) ? document.querySelector('.js-a11y-cards') : null;

if( container !== null ) {
    new A11yCards(container);
}
