<?php 
    $modifier       = $img['modifier'] ?? '';
    $base_path      = $img['base_path'] ?? '';
    $file_extension = $img['file_extension'] ?? '.jpg';
    $srcset         = $img['srcset'] ?? [];
    $sizes          = $img['sizes'] ?? [];
    $srcset_length  = count($srcset);
    $src_fallback   = ( !empty($srcset) && !empty($base_path) ? $base_path . $srcset[count($srcset) - 1] . $file_extension: '');
    $src            = $img['src'] ?? $src_fallback;
    $alt            = $img['alt'] ?? ' ';

    // 1. Creating a string for of srcset values
    $srcset_compiled = Utils\create_srcset($base_path, $srcset, $file_extension);


    // 2. Creating a string of sizes values
    $sizes_compiled = Utils\create_sizes($sizes);

?>

<?php if( !empty($src) && !empty($alt) ) : ?>
    <img
        class="<?= $modifier; ?> lazyload"
        data-srcset="<?= $srcset_compiled; ?>"
        data-sizes="<?= $sizes_compiled; ?>"
        data-src="<?= $src; ?>"
        alt="<?= $alt; ?>"
    >
<?php endif; ?>    