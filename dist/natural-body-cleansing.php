<?php 
    include_once 'partials/core/boot.php';
    include_once 'data/articles.php';

    $data            = $articles_data[2] ?? [];
    $billboard_img   = $data['large_img'] ?? [];
    $article_heading = $data['heading'] ?? '';
    $article_excerpt = $data['excerpt'] ?? '';
?>

<?php if( !empty($data) && !empty($billboard_img) ) : ?>
    <?= Utils\nb_load_template_part('partials/page-templates/blog', [
        'meta_data' => [
            'description' => $article_excerpt
        ],
        'billboard' => [
            'billboard_modifier' => '',
            'billboard_alt'      => 'picture of woman on blue sky',
            'background_img'     => $billboard_img
        ],
        'profile'   => [
            'profile_img'         => '',
            'profile_alt'         => 'Renew Life Logo',
            'profile_title'       => 'Renew Life Team',
            'profile_description' => ''
        ],
        'long_form_copy' => [
            'article'   => 'natural-body-cleansing',
            'heading'   => $article_heading
        ],
        'bio'    => [
            'profile_img'         => '',
            'profile_alt'         => 'Renew Life Logo',
            'profile_title'       => 'Renew Life Team',
            'profile_description' => '',
            'icons'               => [
                [ 
                    'icon' => Utils\get_img_dir("svg/social-share/facebook.svg"),
                    'link' => 'https://www.facebook.com/UKRenewLife/',
                    'vh'   => 'RenewLife Facebook account'
                ],
                [
                    'icon' => Utils\get_img_dir("svg/social-share/instagram.svg"),
                    'link' => 'https://www.instagram.com/ukrenewlife/',
                    'vh'   => 'RenewLife Instagram account'
                ] 
            ],
            'profile_copy'     => 'Our team of Digestive Care experts is committed to empowering consumers to achieve optimal health every day. Since 2002, the Renew Life Team has been dedicated to educating the public about digestive health, cleansing, nutrition, and well-being. As leaders in the natural health field, we go the extra mile to ensure we are delivering valuable resources and helpful information to inspire better health and well-being for your entire family.',
        ],
        'related_articles' => [
            $articles_data[0],
            $articles_data[1],
        ],
    ]); ?>
<?php endif; ?>   
