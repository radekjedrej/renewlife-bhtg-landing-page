<?php
    $ga_id = getenv('GA_TRACKING_ID') ?? "";
?>

<?php if ( !empty($ga_id) && !empty( Utils\get_node_env() ) && Utils\get_node_env() === 'production' ): ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-65527560-13"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', <?= $ga_id; ?>);
    </script>
<?php endif; ?>
