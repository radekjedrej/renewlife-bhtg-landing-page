<?php
namespace Utils;

/**
 * get_node_env
 *
 * @return [string] Returns the current NODE_ENV value as set in the relavent .env file
 */
function get_node_env() {
    return getenv('NODE_ENV') ?? "";
}


/**
 * get_project_dir
 * @param  string $path Optional path to a file or directory
 * @return string       Your path from the root of the project
 */
function get_project_dir($path = '') {
    return $_SERVER['DOCUMENT_ROOT'] . '/' . $path;
}


/**
 * get_assets_dir
 *
 * @param  string $path Optional path to a file or directory
 * @return string       Your path from the root of the project
 */
function get_assets_dir($path = '') {
    return $_SERVER['DOCUMENT_ROOT'] . '/assets/' . $path;
}


/**
 * get_img_dir
 *
 * @param  string $path Optional path to a file or directory
 * @return string       Your path from the root of the project
 */
function get_img_dir($path = '') {
    return $_SERVER['DOCUMENT_ROOT'] . '/assets/img/' . $path;
}


/**
 * check_asset_manifest
 */
function check_asset_manifest() {
    return ( file_exists("assets-manifest.php") ) || false;
}


/**
 * NB LOAD TEMPLATE PART
 *
 * loads a template part using output buffering where you can
 * optionally include $data to be passed into template
 *
 * @param  string $template_name name of the template to be located
 * @param  array  $data          data to be passed into the template to be included
 */
function nb_load_template_part( $template_name, $data=array() ) {

    if ( !strpos($template_name, '.php') ) {
            $template_name = $template_name . '.php';
    }

    // Optionally provided an assoc array of data to pass to tempalte
    // and it will be extracted into variables
    if ( is_array( $data ) ) {
            extract($data);
    }

    ob_start();
    include $template_name;
    $var = ob_get_contents();
    ob_end_clean();

    return $var;
}


/**
 * CREATE SRCSET
 *
 * @param  string $base_path     Path to the file. Each file should be 'path/file-size' 'img/photo-320.jpg'
 * @param  array  $srcset_sizes  An array of ints for the file sizes of each img
 * @param  string $file_ext      The file extension of the img
 * @return string                A srcset string
 */
function create_srcset(string $base_path, array $srcset_sizes, string $file_ext = '.jpg') {
    // NOTE
    // `use($var)` gives the array method access to variables outside it's scope

    // 1. Creating a new array of srcset values. Concatinating together
    //    $base_path, $size & $file_ext. It's CRITICAL that the file
    //    name ends with `-{size}` for this function to work.
    $srcset_array = array_map( function($size) use($base_path, $file_ext) {
        return $base_path . $size . $file_ext . " " . $size . "w"; 
    }, $srcset_sizes );

    // 2. Converting the array to a commer seperated string
    return implode(", ", $srcset_array);
}


/**
 * CREATE SIZES
 *
 * @param  array  $sizes A multi-dimentional array of up to three values needed for sizes attr
 * @return string A string of sizes values
 */
function create_sizes(array $sizes) {
    // 1. Create a new array of values
    $sizes_array = array_map( function($sizes) {

        // 1.1 Creating a value for;
        //
        //      * Breakpoint value. This needs to include a unit as part of the string.        
        //      * If the breakpoint is `min-width` or `max-width`. By default it's `max-width`.
        //      * The size of the image at the breakpoint. This needs to include a unit as part of the string 

        $bp      = $sizes['bp'] ?? "";
        // $min_max = ( $sizes['max_width'] === false ? 'min-width' : 'max-width');
        $size    = $sizes['size'] ?? "";

        
        if( !empty($bp) && !empty($min_max) && !empty($size) ) {
            // 1.2 Creating a sizes string
            return "(" . $min_max . ": " . $bp . ") " . $size;
        } elseif( !empty($size) && empty($bp) ) {
            // 1.3 Creating just a size value
            return $size;
        } else {
            return "";
        }
    },$sizes);

    // 2. Converting the array to a commer seperated string
    return implode(", ", $sizes_array);
}


/**
 * COMPILE RESPONSIVE IMG ATTRS
 *
 * @param  array $img_data
 * @return array
 */
function compile_responsive_img_attrs(array $img_data) {
    $base_path      = $img_data['base_path'] ?? '';
    $file_extension = $img_data['file_extension'] ?? '.jpg';
    $srcset         = $img_data['srcset'] ?? [];
    $sizes          = $img_data['sizes'] ?? [];

    // 1. Creating a string for of srcset values
    $srcset_compiled = create_srcset($base_path, $srcset, $file_extension);

    // 2. Creating a string of sizes values
    $sizes_compiled = create_sizes($sizes);
    
    return [
        'srcset' => $srcset_compiled,
        'sizes'  => $sizes_compiled
    ];
}
