module.exports = {
    plugins: [
        require('autoprefixer'),
        require('postcss-list-style-safari-fix')
    ]
};
