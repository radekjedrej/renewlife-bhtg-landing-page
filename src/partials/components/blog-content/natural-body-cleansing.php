<?php
  $modifier  = !empty($modifier) ? $modifier : "";
?>

<div class="long-form-copy <?=$modifier?>">
    <header>
        <h1 class="heading--bravo heading--primary landmark text--uppercase">
            Natural Body Cleansing: What are the Benefits and Where to Begin?
        </h1>
    </header>

    <p class="landmark">Each day we are confronted by harmful toxins in the air we breathe, the food and water we consume, and even in the products we use every day to make our lives more convenient. Over time, those toxins can build up in the body and impact our overall health. For this reason, many health experts recommend regular internal cleansing to help the body more efficiently eliminate toxins and waste.</p>

    <p class="landmark">While there are plenty of herbal cleansing formulas to choose from, beginners may benefit from a total-body formula designed for first-time cleansers. Such a program will typically last about two weeks and include gentler whole herbs to provide natural support for each of the body’s elimination organs—including the kidneys, liver, lymphatic system, and bowel (or colon). After that, you may wish to move on to a more advanced cleanse. These can last up to a month and will often contain deep-cleansing herbal extracts to support detoxification and help restore digestive regularity.</p>

    <h2 class="heading--charlie heading--primary text--uppercase landmark--quarter">Helpful Tips to Remember</h3>

    <p class="landmark">During any type of cleanse, remember that your goal is to support your detoxification processes while helping your body maintain a healthy balance of nutrients. Your daily diet should provide support for your organs of elimination as well as overall nourishment for your body, which is why choosing the right foods while cleansing is also important. Below are some quick tips to help you get started:</p>

    <ul class="landmark--double">
        <li>Eat plenty of “living foods” such as raw, organic fruits and veggies (veggies may also be lightly steamed) in addition to fermented foods that contain beneficial live bacteria cultures such as yoghurt, kefir, miso and/or tempeh.</li>
        <li>Choose lean protein sources such as organic, natural, or free-range chicken, turkey, beef, fish, tofu, and eggs.</li>
        <li>Consume essential fatty acids such as the beneficial Omega-3 fats found in oily fish and certain nuts and seeds.</li>
        <li>Opt for well-cooked grains from sources such as millet, buckwheat, khorasan wheat, amaranth, quinoa, spelt, teff, rye, basmati rice, wild rice, and brown rice—all of which provide an excellent source of fibre while cleansing.</li>
        <li>Drink plenty of purified water to help to flush unwanted toxins and waste from the body. A good rule of thumb is to drink at least half your body weight in ounces of purified water every day.</li>
        <li>Try to limit your consumption of sugar (both table sugar and hidden refined sugars) and starchy carbohydrates, which may feed unhealthy microbes in the gut.</li>
    </ul>

    
</div>