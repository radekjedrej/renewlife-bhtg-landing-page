<?php 
    include_once 'partials/core/boot.php';
    include_once 'data/articles.php';

    $data            = $articles_data[0] ?? [];
    $billboard_img   = $data['large_img'] ?? [];
    $article_heading = $data['heading'] ?? '';
    $article_excerpt = $data['excerpt'] ?? '';
?>

<?php if( !empty($data) && !empty($billboard_img) ) : ?>
    <?= Utils\nb_load_template_part('partials/page-templates/blog', [
        'meta_data' => [
            'description' => $article_excerpt
        ],
        'billboard' => [
            'billboard_modifier' => '',
            'billboard_alt'      => 'blue surface with notes pen citrus and shoes',
            'background_img'     => $billboard_img
        ],
        'profile'   => [
            'profile_img'         => '/assets/img/jpg/dr-sara.jpg',
            'profile_alt'         => 'dr sara celik',
            'profile_title'       => 'DR. SARA CELIK',
            'profile_description' => 'Neurophathic Doctor'
        ],
        'long_form_copy' => [
            'article'   => 'five-simple-ways',
            'heading'   => $article_heading
        ],
        'bio'    => [
            'profile_img'         => '/assets/img/jpg/dr-sara.jpg',
            'profile_alt'         => 'dr sara celik',
            'profile_title'       => 'DR. SARA CELIK',
            'profile_description' => 'Neurophathic Doctor',
            'icons'               => [
                [ 
                    'icon' => Utils\get_img_dir("svg/social-share/facebook.svg"),
                    'link' => 'https://www.facebook.com/drsaradetox',
                    'vh'   => 'dr sara celik Facebook account'
                ],
                [
                    'icon' => Utils\get_img_dir("svg/social-share/twitter.svg"),
                    'link' => 'https://twitter.com/drsaradetox',
                    'vh'   => 'dr sara celik Twitter account'
                ] 
            ],
            'profile_copy'     => 'Dr. Sara Celik is a Naturopathic Doctor and Homeopathic Master Clinician with over 15 years of experience in the health and wellness industry. With her extensive clinical and educational background, she is a sought-after speaker and health expert that has contributed to various magazines and television programs. As National Spokesperson for Renew Life Canada, Dr. Sara serves as an authority on educating the public about digestive health, detoxification and an integrative approach to optimal wellness.',
        ],
        'related_articles' => [
            $articles_data[1],
            $articles_data[2],
        ],
    ]); ?>
<?php endif; ?>
