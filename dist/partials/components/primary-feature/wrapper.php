<?php 
    include_once 'data/articles.php';

    $articles          = ( !empty($articles_data) ? $articles_data : '' );
    $heading           = $heading ?? 'Article about gut health';
    $heading_modifiers = $heading_modifiers ?? 'vh';
?>

<?php if( !empty($articles) ) : ?>
    <section class="landmark">
        <h2 class="<?= $heading_modifiers ?>"><?= $heading ?></h2>

        <ul class="primary-feature-list js-a11y-cards">
            <?php foreach($articles as $article) : ?>
                <li class="primary-feature-list__item">
                    <?= Utils\nb_load_template_part('partials/components/primary-feature/primary-feature', [
                        'data' => $article
                    ]); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>
<?php endif; ?>
