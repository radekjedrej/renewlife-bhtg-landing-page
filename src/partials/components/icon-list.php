<?php 
    $facts = [
        [
            "heading" => "Eat a healthy diet",
            "icon"    => Utils\get_img_dir("svg/icons/icon-apple.svg")
        ],
        [
            "heading" => "Stay physically active",
            "icon"    => Utils\get_img_dir("svg/icons/icon-bike.svg")
        ],
        [
            "heading" => "Drink plenty of water",
            "icon"    => Utils\get_img_dir("svg/icons/icon-water.svg")
        ],
        [
            "heading" => "take live bacteria supplements",
            "icon"    => Utils\get_img_dir("svg/icons/icon-probiotics.svg")
        ]
    ];
?>

<section class="band band--double band--no-desktop band--secondary landmark--double">
    <header class="container container--reduced">
        <h2 class="heading--bravo heading--section heading--primary landmark">4 ways to support healthy digestion every day</h2>
    </header>
    
    <div class="container">
        <ul class="icon-list">
            <?php foreach($facts as $fact) : ?>
                <?php 
                    $heading = $fact['heading'] ?? '';
                    $icon    = $fact['icon'] ?? '';
                ?>
    
                <?php if( $heading && $icon ) : ?>
                    <li class="icon-list__item">
                        <?= file_get_contents($icon); ?>
                        <h3 class="heading--delta heading--section heading--primary">
                            <?= $heading; ?>
                    </h3>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</section>