<?php
$meta_description    = $meta_data['description'] ?? '';

$billboard_modifier  = !empty($billboard["billboard_modifier"]) ? $billboard["billboard_modifier"] : "";
$billboard_alt       = !empty($billboard["billboard_alt"]) ? $billboard["billboard_alt"] : "";
$billboard_img       = !empty($billboard["background_img"]) ? $billboard["background_img"] : "";

$profile_img         = !empty($profile["profile_img"]) ? $profile["profile_img"] : "";
$profile_alt         = !empty($profile["profile_alt"]) ? $profile["profile_alt"] : "";
$profile_title       = !empty($profile["profile_title"]) ? $profile["profile_title"] : "";
$profile_description = !empty($profile["profile_description"]) ? $profile["profile_description"] : "";

$article             = !empty($long_form_copy["article"]) ? $long_form_copy["article"] : "";
$article_heading     = $long_form_copy["heading"] ?? '';

$bio_img             = !empty($bio["profile_img"]) ? $bio["profile_img"] : "";
$bio_alt             = !empty($bio["profile_alt"]) ? $bio["profile_alt"] : "";
$bio_title           = !empty($bio["profile_title"]) ? $bio["profile_title"] : "";
$bio_description     = !empty($bio["profile_description"]) ? $bio["profile_description"] : "";
$bio_copy            = !empty($bio["profile_copy"]) ? $bio["profile_copy"] : "";

$bio_icons_1         = !empty($bio["icons"]) ? $bio["icons"][0]["icon"] : "";
$bio_icons_2         = !empty($bio["icons"]) ? $bio["icons"][1]["icon"] : "";
$bio_link_1          = !empty($bio["icons"]) ? $bio["icons"][0]["link"] : "";
$bio_link_2          = !empty($bio["icons"]) ? $bio["icons"][1]["link"] : "";
$bio_vh_1            = !empty($bio["icons"]) ? $bio["icons"][0]["vh"] : "";
$bio_vh_2            = !empty($bio["icons"]) ? $bio["icons"][1]["vh"] : "";

?>

<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <title><?= $article_heading ?> | Being Human Takes Guts</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <?php if( !empty($meta_description) ) : ?>
            <meta name="description" content="<?= $meta_description; ?>">
        <?php endif; ?>

        <?php include_once 'partials/core/head.php'; ?>
    </head>
    <body>
        <div class="loading-spinner wf-loader js-loading-spinner" role="alert" aria-live="assertive">
            <?= file_get_contents("assets/img/svg/renew-life-loader.svg"); ?>

            <!-- <p class="wf-loader__copy js-loading-spinner-copy">Content is loading...</p> -->
        </div>

        <?= Utils\nb_load_template_part('partials/core/header', [
            'heading' => 'Ready Frontend Boiletplate'
        ]); ?>

        <main id="main-content" class="main">

            <?= Utils\nb_load_template_part('partials/components/billboard', [
                'modifier'       => $billboard_modifier . " billboard--reduced",
                'alt'            => $billboard_alt,
                'header'         => '',
                'preheader'      => '',
                'background_img' => $billboard_img
            ]); ?>

            <article class="single-post" id="article-content">

                <div class="profile single-post__profile">

                    <?= Utils\nb_load_template_part('partials/components/profile', [
                        'modifier'            => '',
                        'profile_img'         => $profile_img,
                        'profile_alt'         => $profile_alt,
                        'profile_title'       => $profile_title,
                        'profile_description' => $profile_description
                    ]); ?>

                    <?php // Utils\nb_load_template_part('partials/components/social-share', [
                        // 'icons' => [
                        //     [ 
                        //         'icon' => Utils\get_img_dir("svg/social-share/facebook.svg"),
                        //         'link' => '#',
                        //         'vh'   => 'share this post on facebook'
                        //     ],
                        //     [
                        //         'icon' => Utils\get_img_dir("svg/social-share/twitter.svg"),
                        //         'link' => '#',
                        //         'vh'   => 'share this post on twitter'
                        //     ],
                        //     [
                        //         'icon' => Utils\get_img_dir("svg/social-share/pinterest.svg"),
                        //         'link' => '#',
                        //         'vh'   => 'share this post on pinterest'
                        //     ]                       
                        // ]
                    // ]); ?>

                </div>  

                <div class="single-post__copy">
                    <nav class="breadcrumbs" aria-label="Breadcrumb">
                        <ol class="breadcrumbs__list">
                            <li class="breadcrumbs__item">
                                <a href="/" class="breadcrumbs__link">
                                    Home
                                </a>
                            </li>
                            <li class="breadcrumbs__item">
                                <a href="#article-content" class="breadcrumbs__link" aria-current="page">
                                    <?= $article_heading; ?>
                                </a>
                            </li>
                        </ol>
                    </nav>

                    <?= Utils\nb_load_template_part("partials/components/blog-content/$article"); ?>

                    <?= Utils\nb_load_template_part('partials/components/bio', [
                        'modifier'            => 'long-form-copy__separator',
                        'bio_img'         => $bio_img,
                        'bio_alt'         => $bio_alt,
                        'bio_title'       => $bio_title,
                        'bio_description' => $bio_description,
                        'icons'               => [
                            [ 
                                'icon' => $bio_icons_1,
                                'link' => $bio_link_1,
                                'vh'   => $bio_vh_1
                            ],
                            [
                                'icon' => $bio_icons_2,
                                'link' => $bio_link_2,
                                'vh'   => $bio_vh_2
                            ]             
                        ],
                        'bio_copy'     => $bio_copy,
                    ]); ?>


                </div>

            </article>

            <div class="single-post__related-articles">
                <div class="single-post__related-articles-inner">
                    <?= Utils\nb_load_template_part('partials/components/primary-feature/wrapper', array(
                        'articles_data'     => $related_articles,
                        'heading'           => 'Related articles',
                        'heading_modifiers' => 'heading--charlie heading--section heading--primary',
                    )); ?>
                </div>
            </div>

        </main>

        <?= Utils\nb_load_template_part('partials/core/footer'); ?>    

        <?php include_once 'partials/core/javascript-assets.php'; ?>
        <?php include_once 'partials/core/google-analytics.php'; ?>
    </body>
</html>
