<?php
    $gtag_id = getenv('GTAG_TRACKING_ID') ?? "";
?>

<?php //if ( !empty($gtag_id) && !empty( Utils\get_node_env() ) && Utils\get_node_env() === 'production' ): ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtag_id ?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php //endif; ?>
