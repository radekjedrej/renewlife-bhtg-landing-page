<?php
    // 1. Getting favicon HTML
    $favicons_json = ( file_exists("favicons-data.json") === true ) ? file_get_contents("favicons-data.json") : false;
    $json          = json_decode($favicons_json, TRUE) ?? "";
    $icons         = $json['html'] ?? [];

    if( $icons ) {
        foreach ($icons as $favicon) {
            echo $favicon;
        }
    }

    // 2. Inserting link to css file
    $assets_manifest = Utils\check_asset_manifest();

    // 3. Getting path to `main.css` file
    $css_main = ( !empty($assets_manifest) ) ? webpackAssets::$cssFiles['main'] : false;
?>

<?= Utils\nb_load_template_part('partials/core/site-loader-styles'); ?>

<?php if ( $css_main ): ?>
    <link rel="stylesheet" href="<?= $css_main; ?>">
<?php endif; ?>

<?= Utils\nb_load_template_part('partials/core/js-detection'); ?>
<?= Utils\nb_load_template_part('partials/core/bugherd'); ?>
<?= Utils\nb_load_template_part('partials/core/google-tag-manager'); ?>
