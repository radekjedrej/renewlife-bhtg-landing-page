import Vimeo from '../vendor/vimeo-player';

const buyNowNodes = (document.querySelectorAll('.product-slider__button--buy-now') !== null) ? document.querySelectorAll('.product-slider__button--buy-now') : null;
const videoNode = (document.querySelector('.js-video-embed') !== null) ? document.querySelector('.js-video-embed') : null;

if( buyNowNodes !== null ) {
    buyNowNodes.forEach( (button) => {
        if( typeof ga === 'function' ) {
            // Grab the product name to use as an event label. Falls back to 'unknown'.
            const label = (button.dataset.forProduct !== null) ? button.dataset.forProduct : 'unknown';
            const buttonPress = {
                eventCategory: 'Slider',
                eventAction: 'click',
                eventLabel: label,
            };

            button.onclick = function() {
                ga('send', 'event', buttonPress);
            };
        }
    });
}

if ( typeof Vimeo === 'function' && videoNode !== null ) {
    const player = new Vimeo(videoNode);

    // Use the Vimeo player API to track video plays.
    player.on('play', function() {
        if( typeof ga === 'function' ) {
            const videoPlay = {
                eventCategory: 'Video',
                eventAction: 'play',
            };

            ga('send', 'event', videoPlay);
        }
    });
}
