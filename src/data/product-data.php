<?php

    // Used to store the products for the product slider. The index of each product is used as its identifier.
        // "name"        => The product name, displayed as a title.
        // "image"       => The image name, as listed in /assets/img/products/
        // "url"         => The Amazon page URL for the product.
        // "highlights"  => An array of highlight IDs - see `data/highlight-data.php` for more.
        // "about"       => The product description.
        // "features"    => The product features.
        // "ingredients" => The product ingredients.


    $img_setup = [
        'modifier'  => 'product-slider__product-image',
        'srcset'    => [150, 200, 300, 350, 400, 700],
        'sizes'     => [
            [
                'bp'   => '320px',
                'size' => '44vw'
            ],
            [
                'bp'   => '600px',
                'size' => '38vw'
            ],
            [
                'bp'   => '900px',
                'size' => '30vw'
            ],
            [
                'bp'   => '1200px',
                'size' => '24vw'
            ],
            [
                'bp'        => '1201px',
                'size'      => '350px',
                'max_width' => false
            ]
        ]
    ];    

    $img_base_path = 'assets/img/products/artworked/';

    $allProducts = array(
        0 => array (
            "name"        => "Ultimate Flora Critical Care",
            "quantity"    => "50 Billion Bacteria, 30 Vegetable Capsules",
            "about"       => "<p>
                                  Renew Life® Ultimate Flora™ Critical Care 50 Billion is a potent formula with 10 specially selected Bifidobacterium and Lactobacillus strains to reflect the gut's natural diversity. Many beneficial live bacteria do not survive the high acid environment of the stomach. For this reason Ultimate Flora live bacteria suppliments have a delayed release capsule  proven to ensure the live bacteria are not destroyed in the stomach and arrive alive to the alkaline intestinal tract where they are released. Available in both 14 and 30 vegetable capsules.
                              </p>",
            "features"    => "<ul>
                                  <li>ULTIMATE FLORA live bacteria supplement: live bacteria supplements with 50 billion cultures.</li>
                                  <li>STRAINS: 10 extensively researched strains of live bacteria.</li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Live bacteria consisting of the following:  Bifidobacterium lactis, Bifidobacterium breve, Bifidobacterium longum,  Lactobacillus acidophilus, Lactobacillus casei, Lactobacillus plantarum, Lactobacillus paracasei, Lactobacillus salivarius, Lactobacillus rhamnosus, Lactobacillus bulgaricus, L-leucine, Vegetable capsule (vegetable fibre and water) and celluose.
                              </p>",
            "highlights"  => [4, 5, 6, 0, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'critical-care-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-Ultimate-Flora-billion/dp/B00DJ5RYRU",
            "theme"       => "blue",
        ),
        1 => array (
            "name"        => "Ultimate Flora Women's Formula",
            "quantity"    => "50 Billion Bacteria, 30 Vegetable Capsules",
            "about"       => "<p>
                                  Renew Life® Ultimate Flora™ Women's Formula is formulated to help maintain a healthy balance of vaginal and urinary tract bacteria. Each capsule contains 45 billion live Lactobacillus cultures and 5 billion live Bifidobacteria, mirroring the prevalence of Lactobacilli  in the vagina. The Ultimate Flora Women's Formula uses a delayed release capsule to ensure safe passage of the beneficial bacteria through the stomach acid.
                              </p>",
            "features"    => "<ul>
                                  <li>ULTIMATE FLORA live bacteria supplement: live bacteria supplements with 50 billion cultures.</li>
                                  <li>STRAINS: 10 extensively researched strains of live bacteria.</li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Live bacteria consisting of the following:  Lactobacillus acidophilus, Lactobacillus rhamnosus, Lactobacillus plantarum, Bifidobacterium lactis, Lactobacillus gasseri, Lactobacillus casei,  Lactobacillus salivarius, Lactobacillus paracasei, Lactobacillus brevis, Bifidobacterium longum,  L-leucine, Vegetable capsule (vegetable fibre and water) and celluose.
                              </p>",
            "highlights"  => [4, 5, 6, 0, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'womens-care-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-Ultimate-Flora-Complete/dp/B00K5M1KMA",
            "theme"       => "pink",
        ),
        2 => array (
            "name"        => "Ultimate Flora Colon Care",
            "quantity"    => "80 Billion Bacteria, 30 Vegetable Capsules",
            "about"       => "<p>
                                  Renew Life® Ultimate Flora™ Colon Care live bacteria supplement 80 Billion is a high-potency formula with 14 scientifically studied strains. Each capsule provides 52 billion active cultures of Bifidobacteria found in the large intestine and 28 billion active cultures of Lactobacilli found in the small intestine. Many beneficial live bacteria do not survive the high acid environment of the stomach. For this reason Ultimate Flora live bacteria suppliments have a delayed release capsule proven to ensure the live bacteria are not destroyed in the stomach and arrive alive to the alkaline intestinal tract where they are released.
                              </p>",
            "features"    => "<ul>
                                  <li>ULTIMATE FLORA live bacteria supplement: live bacteria supplements with 80 billion cultures.</li>
                                  <li>STRAINS: 14 extensively researched strains of live bacteria.</li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Live bacteria consisting of the following: Bifidobacterium lactis (3 strains), Lactobaccillus acidophilus, Bifidobacterium breve, Bifidobacterium infactis, Lactobacillus salivarius, Lactobacillus plantarum, Bifidobacterium longum, Lactobacillus rhamnosus, Bifidobacterium bi dum, Lactobacillus casei, Lactobacillus gasseri, Lactobacillus paracasei), L-leucine, Vegetable capsule (vegetable fibre and water) and cellulose.
                              </p>",
            "highlights"  => [4, 5, 6, 0, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'colon-care-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-Ultimate-Flora-Billion/dp/B071PBHV89",
            "theme"       => "green",
        ),
        3 => array (
            "name"        => "Ultimate Flora Ultra Potent",
            "quantity"    => "100 Billion Bacteria, 30 Vegetable Capsules",
            "about"       => "<p>
                                  Renew Life® Ultimate Flora™ Ultra Potent, 100 Billion live bacteria supplement is a high-potency, once daily live bacteria supplement formuled with 12 scientifically studied Bifidobacterium and Lactobacillus strains designed to restore digestive balance. Each capsule provides 60 billion active cultures of Bifidobacterium and 40 billion active cultures of Lactobacilli. Ultimate Flora Probiotics use a delayed release capsule that ensures the bacteria will survive past the harsh, acidic environment of the stomach and arrive alive in the intestinal tract.
                              </p>",
            "features"    => "<ul>
                                  <li>ULTIMATE FLORA live bacteria supplement: live bacteria supplements with 100 billion cultures.</li>
                                  <li>STRAINS: 10 scientifically studied Bifidobacterium and Lactobacillus strains </li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Live bacteria consisting of the following: Bifidobacterium lactis (3 strains), Lactobacillus acidophilus, Bifidobacterium longum, Lactobacillus casei, Lactobacillus paracesi, Lactobacillus plantarum, Lactobacillus salivarius, Lactobacillus rhamnosus,  Bifidobacterium bifidum, Bifidobacterium infactis,  L-leucine, Vegetable capsule (vegetable fibre and water) and celluose. 
                              </p>",
            "highlights"  => [4, 5, 6, 0, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'ultra-potent-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-Ultimate-Probiotic-Capsules/dp/B07LBLLVNR",
            "theme"       => "purple",
        ),
        4 => array (
            "name"        => "FibreSMART",
            "quantity"    => "120 Vegetable Capsules",
            "about"       => "<p>
                                  FibreSMART is a flax based fibre containing a blend of roughly 50% soluble fibre and 50% insoluble fibre to bind toxins and increase elimination, without the constipating side effects created by other fibres. FibreSMART also contains herbal ingredients L-Glutamine and Triphala that work to soothe and support the intestinal tract. FibreSMART is available in both capsule and powder form.
                              </p>",
            "features"    => "<ul>
                                  <li>FIBRESMART: a flax based fibre containing a blend of roughly 50% soluble fibre and 50% insoluble fibre in capsule form</li>
                                  <li>FIBRE: Source of Fibre with L-Glutamine and Triphala</li>
                                  <li>HERBAL: Contains herbal ingredients L-Glutamine and Triphala </li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Flax Seed Powder, organic (Linum usitatissimum), Acacia Gum powder, organic (Acacia senegal), Vegetable Capsule Shell (hydroxypropyl methylcellulose, water), Guar Gum powder (Cyamopsis tetragonoloba), L-Glutamine, Marshmallow Root Powder (Althaea officinalis), Slippery Elm Bark (Ulmus rubra), Belleric Myrobalan Fruit powder (Terminalia bellirica), Chebulic Myrobalan powdert (Terminalia chebula), Indian Gooseberry Fruit powder (Emblica officinalis), Bulking Agent: Vegetable Cellulose (hydroxypropyl methylcellulose, water).
                              </p>",
            "highlights"  => [7, 9, 2, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'fibre-caps-']
            ),
            "url"         => "https://www.amazon.co.uk/RENEW-LIFE-FIBRESMART-Renew-Life/dp/B0798PZDG2",
            "theme"       => "purple",
        ),
        5 => array (
            "name"        => "FibreSMART Powder",
            "quantity"    => "227g",
            "about"       => "<p>
                                  FibreSMART is a flax based fibre containing a blend of roughly 50% soluble fibre and 50% insoluble fibre to bind toxins and increase elimination, without the constipating side effects created by other fibres. FibreSMART also contains herbal ingredients L-Glutamine and Triphala that work to soothe and support the intestinal tract. FibreSMART is available in both capsule and powder form.
                              </p>",
            "features"    => "<ul>
                                  <li>FIBRESMART: a flax based fibre containing a blend of roughly 50% soluble fibre and 50% insoluble fibre in powder form</li>
                                  <li>FIBRE: Source of Fibre with L-Glutamine and Triphala</li>
                                  <li>HERBAL: Contains herbal ingredients L-Glutamine and Triphala</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Flax Seed Powder, organic (Linum usitatissimum), Acacia Gum Powder, organic (Acacia senegal), Guar Gum Powder (Cyamopsis tetragonoloba), L-Glutamine, Marshmallow Root Powder (Althaea officinalis), Slippery Elm Bark Powder (Ulmus rubra), Belleric Myrobalan Fruit Powder (Terminalia bellirica), Chebulic Myrobalan Fruit Powder (Terminalia chebula), Indian Gooseberry Fruit Powder (Emblica officinalis), Sweetener: Steviol Glycosides (Stevia Rebaudiana).
                              </p>",
            "highlights"  => [7, 9, 2, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'fibre-smart-powder-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-FibreSMART-Powder-227grams/dp/B00UYBDLV0",
            "theme"       => "purple",
        ),
        6 => array (
            "name"        => "CandiGONE",
            "quantity"    => "60 Capsules",
            "about"       => "<p>
                                  Renew Life® CandiGONE™ is a 15 day programme containing a blend of natural herbs and ingredients to aid the defense against harmful organisms.
                              </p>",
            "features"    => "<ul>
                                  <li>CANDIGONE: A 15 day programme to aid the defense against harmful organisms.</li>
                                  <li>PROGRAMME: a 15 day programme of 60 capsules </li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Grapefruit Seed Extract Powder (Citrus paradisi), Barberry Root Powder (Berberis vulgaris), Clove Bud Powder (Syzygium aromaticum), Garlic Bulb Extract Powder (Allium sativum), Olive Leaf Powder (Olea europa), Peppermint Leaf Powder (Mentha piperita), Vegetable Capsule Shell (hydroxypropyl methylcellulose, water)
                              </p>",
            "highlights"  => [6, 10, 15, 9, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'candigone-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-Candigone-60s-UK/dp/B0798PWWGQ",
            "theme"       => "sky-blue",
        ),
        7 => array (
            "name"        => "LiverDTX",
            "quantity"    => "120 Capsules",
            "about"       => "<p>
                                  Renew Life® LiverDTX™ is  30-day, two-part cleansing program containing Dandelion Root, which promotes liver function, and N-Acetyle-Cystein (NAC). 
                              </p>",
            "features"    => "<ul>
                                  <li>LIVERDTX: 30 day cleansing programme containing Dandelion Root, which promotes liver function, and N-Acetyl-cysteine (NAC).</li>
                                  <li>PROGRAMME: a 30 day programme of 2x 60 capsules </li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  <b>LiverDTX 1</b>: Dandelion Root Extract (Taraxacum officinale), Tumeric Root Extract (Curcuma longa), Artichoke Leaf Extract (Cynara scolymus), Tinospora Cordifolia Stem powder, Vegetable Capsule Shell, Bulking Agent: Cellylose (hydroxypropyl methylcellulose, water).<br /><br /><b>LiverDTX 2</b>: L-Methionine powder, N-Acetyl-Cysteine powder, DL-Alpha Lipoic Acid powder, L-Taurine powder, Vegetable Capsule Shell, Bulking agent: Cellylose (hydroxypropyl methylcellulose, water).
                              </p>",
            "highlights"  => [6, 11, 14, 12, 8],
            "image"       => "liver-DTX-x1.jpg",
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'liver-DTX-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-LiverDETOX-kit/dp/B00RYA27UK",
            "theme"       => "green",
        ),
        8 => array (
            "name"        => "ParaGONE",
            "quantity"    => "60 Capsules",
            "about"       => "<p>
                                  Renew Life® ParaGONE™ is a 15 day programme formulated with Wormwood Leaf as a vermifuge (to expel intestinal worms). 
                              </p>",
            "features"    => "<ul>
                                  <li>PARAGONE: ParaGONE is a 15 day programme formulated with Wormwood Leaf as a vermifuge (to expel intestinal worms).</li>
                                  <li>PROGRAMME: a 15 day programme of 60 capsules and 30ml Tincture</li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no peanuts, sesame seeds, mustard, wheat, celery, lactose, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  <b>ParaGONE 1</b>: Wormwood Leaf Extract (Artemisia absinthium), Caprylic Acid powder (Magnesium caprylate), Garlic Bulb Extract (Allium sativum), Thyme Leaf Extract (Thymus vulgaris), Vegetable Capsule Shell (hydroxypropylmethylcellulose, water), Black WALNUT Hull Powder (Juglans nigra), Clove Bud powder (Syzygium aromaticum), Pippli Seed powder (Piper longum), Quassia Wood Extract (Picrasma excelsa) <br /><br /><b>ParaGONE 2</b>: Carrier: Filtered Water, Carrier: Ethyl Alcohol, Black WALNUT Hulls (Juglans nigra), Clove Bud (Syzygium aromaticum), Marshmallow Root (Althaea officinalis), Orange Peel (Citrus sinensis), Wormwood Leaf (Artemisia abisinthium).
                              </p>",
            "highlights"  => [6, 10, 15, 9, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'para-gone-']
            ),
            "url"         => "https://www.amazon.co.uk/Paragone-Kit-UK-Renew-Life/dp/B0798Q7NLX",
            "theme"       => "blue",
        ),
        9 => array (
            "name"        => "DigestMORE Regular Strength",
            "quantity"    => "90 Vegetable Capsules",
            "about"       => "<p>
                                  Renew Life®DigestMORE™ is a plant based digestive enzyme formula designed to support overall function of the digestive system .When DigestMORE is taken with a meal, it works to break down food products into their smallest usable components (nutrients). This allows the body to absorb the maximum amount of nutrients from your meal. When food is effectively broken down, the overall function of the digestive system is improved, including a reduction in the amount of gas produced. DigestMORE also contains ingredients that are essential to repair and soothe the digestive system.
                              </p>",
            "features"    => "<ul>
                                  <li>DIGESTMORE: A powerful blend of digestive enzymes that contains Ginger Root which helps to support digestion</li>
                                  <li>ENZYMES: Digestive enzymes help ensure the proper breakdown of food allowing the body to absorb the maximum amount of nutrients from your meal. </li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy, crustaceans, shellfish,  fish. </li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  Ginger Root, Rhizome Powder (Zingiber officinale), L-Glutamine, Vegetable Capsule Shell (hydroxypropylmethylcellulose, water), Amylase Enzyme Powder*, Marshmallow Root Powder (Althaea officinalis), Papaya Fruit Powder (Carica papaya), Protease Enzyme Powder*, Malt Diastase Enzyme Powder*, Pectinase Enzyme Powder*, Lipase Enzyme Powder*, Bulking Agent: Vegetable Cellulose (hydroxypropylmethylcellulose, water), Bromelain Enzyme Powder*, Gamma Oryzanol Powder, Lactase Enzyme Powder*, Cellulase Enzyme Powder*, Phytase Enzyme Powder*, Invertase Enzyme Powder*.<br /><br />*Carrier agent: Maltodextrin
                              </p>",
            "highlights"  => [3, 1, 9, 8],
            "image"       => "digestmore-x1.jpg",
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'digestmore-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-DigestMORE-90-Capsules/dp/B00DJ61EP2",
            "theme"       => "green",
        ),
        10 => array (
            "name"        => "IntestiNEW Powder",
            "quantity"    => "162g",
            "about"       => "<p>
                                  Renew Life® IntestiNEW™ powder contains L-Glutamine and N-acetyl-Glucosamine to help rebuild and repair the intestinal tract lining, while also helping to reduce inflammation in the intestinal tract. 
                              </p>",
            "features"    => "<ul>
                                  <li>INTESTINEW: a powerful blend including Ginger Root that contributes to the normal function of the intestinal tract</li>
                                  <li>SUPPORT: Contains ginger root which contributes to the normal function of the digestive tract.</li>
                                  <li>POTENCY: Quality, purity and potency guaranteed through expiration</li>
                                  <li>Contains no nuts, peanuts, sesame seeds, mustard, wheat, celery, gluten, dairy, egg, soy.</li>
                                  <li>Renew Life does not use genetically modified ingredients.
                                  </li>
                              </ul>",
            "ingredients" => "<p>
                                  L-Glutamine, N-Acetylglucosamine (Crab and shrimp shells), Ginger Root Extract (Zingiber o cinale), Gamma Oryzanol Powder, Marigold Flower Powder (Calendula o cinalis), Marshmallow Root Powder (Althaea o cinalis).<br /><br />For allergens, please see ingredients in bold.
                              </p>",
            "highlights"  => [13, 16, 8],
            "image"       => array_merge(
                $img_setup,
                ['base_path' => $img_base_path . 'intestinew-']
            ),
            "url"         => "https://www.amazon.co.uk/Renew-Life-IntestiNEW-Powder-162g/dp/B00DJ6096C",
            "theme"       => "purple",
        )
    );