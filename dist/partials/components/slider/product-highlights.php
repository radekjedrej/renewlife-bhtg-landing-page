<?php 
    include 'data/highlight-data.php';
    $allHighlights = $allHighlights ?? '';
    if( !empty($allHighlights) && !empty($product_highlights) ):
?>

<div class="panel-content__highlights__container band">

    <?php
        foreach($product_highlights as $product_highlight):
            $highlight           = $allHighlights[$product_highlight];
            $highlight_image_url = Utils\get_img_dir('svg/highlights/' . $highlight['image']);
            if( !empty($highlight_image_url) ):
    ?>

            <div class="panel-content__highlights__highlight">
                <?= file_get_contents( $highlight_image_url ); ?>
            </div>

    <?php
            endif;
        endforeach;
    ?>

</div>

<?php
    endif;