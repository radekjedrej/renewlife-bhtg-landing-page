# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

For more information about keeping good change logs please refer to [keep a changelog](https://github.com/olivierlacan/keep-a-changelog).

### ADDED
- CHANGELOG.MD

### CHANGED
- How footer is checking if social networks have a link

### FIXED
- A bug that was super annoying


## [0.1.5] - 2019-1-18
### ADDED
 - Better comments to `_forms.scss`
 - Better styles for `form__err-message`


## [0.1.4] - 2019-1-18
### CHANGE
 - Tweaking form required styles


## [0.1.3] - 2019-1-18
### CHANGE
 - Updating SVG-chevron mixin so its more flexible
 - Updated forms mixins and created some vars
 - Created some new form-container modifiers
 - Refactored custom form elements styles


## [0.1.2] - 2019-1-16
### ADDED
 - A couple of `.env` plugins to we can use `.env` files in the webpack config file as well as exposing it to our JavaScript files.
 - Some new instructions to the `README.md`


## [0.1.1] - 2019-1-16
### ADDED
 - Installing `postcss-list-style-safari-fix`

### FIXED
 - All SCSS linting errors
 - Linting rule for whitespace before `@warn`


## [0.1.0] - 2019-1-11
### ADDED
 - Initial release
