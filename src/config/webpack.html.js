const myPaths = require('./project-paths'),
    settings = require('./project-settings'),
    glob = require('glob');

const HtmlWebpackPlugin = require('html-webpack-plugin'),
    FaviconsWebpackPlugin = require('favicons-webpack-plugin'),
    PurgecssPlugin = require('purgecss-webpack-plugin'),
    ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
    plugins: [
        // Creating a HTML file
        new HtmlWebpackPlugin({
            template: myPaths.indexHtml,
            title: settings.title,
            // templateParameters: {
            //      key: 'Value'
            // }
            // In the template to output the result
            // <%= key %>
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            }
        }),


        // Generates favicons from a single image file
        new FaviconsWebpackPlugin({
            logo: `${myPaths.img}/favicon/favicon.png`,
            background: settings.colors.primary,
            title: settings.title,
            inject: true
        }),


        // Removing unused CSS
        new PurgecssPlugin({
            // Give paths to parse for rules. These should be absolute!
            // By using `glob` we can check multiple files
            paths: glob.sync(
                `${myPaths.partials}/*.**`
            )
        }),

        new ScriptExtHtmlWebpackPlugin({
            inline: ['inline'],
            aysnc: ['main'],
            defaultAttribute: 'async'
        }),
    ]
};
