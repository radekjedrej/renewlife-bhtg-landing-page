<?php
    $allProducts = $allProducts ?? '';
    if( !empty($allProducts) ):
?>
    <div class="glide js-glide--product-slider glide--product-slider">
        <div class="gradient-overlay"></div>
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">

                <?php
                    foreach ($allProducts as $product_id => $product):
                        if( !empty($product['image']) && !empty($product['name']) && !empty($product['quantity']) ):

                            $img        = $product['image'] ?? [];
                            $img['alt'] = $product['name'] . ': ' . $product['quantity'];
                ?>

                            <li class="glide__slide js-glide__slide" data-slide-id="<?= $product_id ?>">
                                <?= Utils\nb_load_template_part('partials/components/shared/responsive-inline-img', [
                                   'img' => $img
                                ]); ?>
                            </li>

                <?php
                        endif;
                    endforeach;
                ?>
                
            </ul>
        </div>
        <div class="glide__arrows" data-glide-el="controls">

            <button class="glide__arrow glide__arrow--left" data-glide-dir="<" aria-label="Move product carousel left">
                <svg class="glide__arrow__icon" width="50" height="50" viewBox="0 0 26 24" fill="none" stroke="#003b71" stroke-width="2" stroke-linecap="square" stroke-linejoin="arcs"><path d="M15 18l-6-6 6-6"/></svg>
            </button>

            <button class="glide__arrow glide__arrow--right" data-glide-dir=">" aria-label="Move product carousel right">
                <svg class="glide__arrow__icon" width="50" height="50" viewBox="0 0 22 24" fill="none" stroke="#003b71" stroke-width="2" stroke-linecap="square" stroke-linejoin="arcs"><path d="M9 18l6-6-6-6"/></svg>
            </button>

        </div>
    </div>

<?php
    endif;