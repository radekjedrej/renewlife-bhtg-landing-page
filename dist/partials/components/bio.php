<?php
  $modifier          = !empty($modifier) ? $modifier : "";
  $bio_img           = !empty($bio_img) ? $bio_img : "";
  $bio_alt           = !empty($bio_alt) ? $bio_alt : "";
  $bio_title         = !empty($bio_title) ? $bio_title : "";
  $bio_copy          = !empty($bio_copy) ? $bio_copy : "";
  $bio_description   = !empty($bio_description) ? $bio_description : "";
  $icons             = !empty($icons) ? $icons : "";

?>


<?php if($bio_title): ?>

    <section class="bio landmark--double <?=$modifier?>">
        
        <?php if($bio_img): ?>
            <img alt="<?=$bio_alt?>" class="bio__img" src="<?=$bio_img?>">
        <?php else: ?>
            <?= file_get_contents( Utils\get_img_dir("svg/renew-life-mark-logo-circle.svg") ); ?>
        <?php endif; ?>

        <div class="bio__wrapper">
            <header class="bio__header">
                <div class="bio__header-inner">
                    <h2 class="bio__title heading--charlie text--uppercase heading--primary">
                        <span class="vh">Information about the author </span><?=$bio_title?>
                    </h2> 
    
                    <ul class="social-share-list social-share-list--bio">
                        <?php foreach($icons as $icon) : ?>
                            <?php if( $icon ) : ?>
                                <li class="social-share-list__item">
                                    <a class="social-share-list__link" href="<?= $icon['link']; ?>" target='_blank'>
                                        <span class="vh"><?= $icon['vh']; ?></span>
                                        <?= file_get_contents($icon['icon']); ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <?php if($bio_description): ?>
                    <span class="bio__description text--small">
                        <?=$bio_description?>
                    </span>
                <?php endif; ?>
            </header>

            <?php if($bio_copy): ?>
                <p class="bio__copy text--small">
                    <?=$bio_copy?>
                </p>
            <?php endif; ?>
        </div>
    </section>

<?php endif; ?>   