<?php

    // Used to store the highlights of each product in the product slider.
        // "name"        => The feature name, displayed as a title.
        // "image"       => The image name, as listed in /assets/img/products/

    $allHighlights = array(
        0 => array(
            "name"  => "Proven Delivery System",
            "image" => "proven-delivery.svg",
        ),
        1 => array(
            "name"  => "Plant Based Enzyme",
            "image" => "enzyme.svg",
        ),
        2 => array(
            "name"  => "Soluble and Insoluble Fibre",
            "image" => "fibre.svg",
        ),
        3 => array(
            "name"  => "Contains Ginger Root",
            "image" => "ginger.svg",
        ),
        4 => array(
            "name"  => "High Potency",
            "image" => "high-potency.svg",
        ),
        5 => array(
            "name"  => "Multi Strain",
            "image" => "multi-strain.svg",
        ),
        6 => array(
            "name"  => "Potency Guaranteed",
            "image" => "potency-guarenteed.svg",
        ),
        7 => array(
            "name"  => "Powder and Capsule Form",
            "image" => "powder.svg",
        ),
        8 => array(
            "name"  => "Quality Guarantee",
            "image" => "quality-guarentee.svg",
        ),
        9 => array(
            "name"  => "Vegetarian and Vegan",
            "image" => "vege.svg",
        ),
        10 => array(
            "name"  => "15 Day Programme",
            "image" => "15-day-2-part.svg",
        ),
        11 => array(
            "name"  => "30 Day Programme",
            "image" => "30-day-2-part.svg",
        ),
        12 => array(
            "name"  => "Dandelion",
            "image" => "dandelion.svg",
        ),
        13 => array(
            "name"  => "Contains L-Glutamine",
            "image" => "l-glutamine.svg",
        ),
        14 => array(
            "name"  => "Contains Milk Thistle",
            "image" => "milk-thistle.svg",
        ),
        15 => array(
            "name"  => "Natural Ingredients",
            "image" => "natural-ingredients.svg",
        ),
        16 => array(
            "name"  => "Contains soothing herbs",
            "image" => "soothing-herbs.svg",
        ),
    );