<?php
   $icons  = !empty($icons) ? $icons : "";
?>

<?php if($icons) : ?>

    <div class="social-share">
        <span class="social-share__preheader">Share article</span>
        <ul class="social-share-list">

            <?php foreach($icons as $icon) : ?>
                <?php if( $icon ) : ?>
                    <li class="social-share-list__item">
                        <a class="social-share-list__link" href="<?= $icon['link']; ?>">
                            <span class="vh"><?= $icon['vh']; ?></span>
                            <?= file_get_contents($icon['icon']); ?>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>

        </ul>
    </div>

<?php endif; ?>