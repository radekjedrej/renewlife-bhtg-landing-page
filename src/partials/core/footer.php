<footer class="footer">
    <div class="footer__inner">
        <h2 class="vh">Footer</h2>

        <div class="footer__item">
            <h3 class="footer__heading">
                Contact us
            </h3>
            <a href="mailto:renewlife@clorox.com" class="footer__email-link">
                renewlife@clorox.com
            </a>
        </div>
        
        <div class="footer__item footer__item--large">
            <a href="/" class="footer__link">
                <span class="vh">Home</span>

                <?= Utils\nb_load_template_part('partials/components/shared/renew-life-mark-logo', [
                    'modifier' => 'footer__logo'
                ]); ?>
            </a>

            <span class="text--white text--small">
                #BeingHumanTakesGuts
            </span>
        </div>

        <?php // Refactor to Use social share component ?>
        <div class="footer__item">
            <h3 class="footer__heading">
                Follow us <span class="vh">Renew Life Socialmedia profiles</span>
            </h3>

            <ul class="footer__social">
                <li class="footer__social-item footer__social-item--facebook">
                    <a href="https://facebook.com/ukrenewlife" class="footer__social-link" rel="noopener noreferrer" target="_blank">
                        <span class="vh">facebook.com</span>/ukrenewlife
                    </a>
                </li>
                <li class="footer__social-item footer__social-item--instagram">
                    <a href="https://instagram.com/ukrenewlife" class="footer__social-link" rel="noopener noreferrer" target="_blank">
                        <span class="vh">instagram.com</span>/ukrenewlife
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>