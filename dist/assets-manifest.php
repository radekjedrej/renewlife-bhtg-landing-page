<?php
/** 
* Built by webpack-php-manifest 
* Class webpackAssets
*/
class webpackAssets {
  static $jsFiles = [
    'inline' => 'inline.e9567e96a362d783406e.js',
    'main' => 'main.6dd7deaaf4ded25f7677.js',
  ];
  static $cssFiles = [
    'main' => 'main.6dd7deaaf4ded25f7677.css',
  ];
}
