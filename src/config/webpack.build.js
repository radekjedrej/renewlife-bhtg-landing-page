/* eslint-disable no-unused-vars */
const webpack = require('webpack'),
    merge = require('webpack-merge'),
    common = require('./webpack.common.js'),
    phpConfig = require('./webpack.php.js');

const myPaths = require('./project-paths'),
    /* eslint-disable no-unused-vars */
    settings = require('./project-settings');

// Plugins
const UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin'),
    ManifestPlugin = require('webpack-manifest-plugin'),
    StyleLintPlugin = require('stylelint-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    ImageminPlugin = require('imagemin-webpack-plugin');
    // BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,


////module.exports = merge(common, {                 // Basic version just to compile assets
// module.exports = merge(common, htmlConfig, {  // Works with HTML files
module.exports = merge(common, phpConfig, {   // Working on php files
    entry: {
        main: [
            // By having .scss file as an entry it will
            // be compiled into it's own `.css` file.
            `${myPaths.scss}/main.scss`,

            `${myPaths.js}/main.js`
        ],
        inline: [
            `${myPaths.js}/behaviour/font-loading-classes.js`,
            // `${myPaths.js}/behaviour/js-detection.js`
        ]
    },

    mode: 'production',

    output: {
        // eslint-disable-next-line
        path: `${myPaths.dist}`,
        // [chunkhash] gives us a unique hash per asset
        filename: '[name].[chunkhash].js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                }]
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: 'css-loader' },
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: `${myPaths.config}`
                            }
                        }
                    },
                    { loader: 'sass-loader' }
                ]
            }
        ]
    },

    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                test: /\.js(\?.*)?$/i,
                cache: true,
                parallel: true,
                sourceMap: true // set to true if you want JS source maps
            }),
            // Minify CSS
            new OptimizeCSSAssetsPlugin({})
        ],
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'main',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                }
            },
            // chunks: 'all'
        },
        // runtimeChunk: 'single'
    },

    plugins: [
        // Removing all old files inside dist directory
        new CleanWebpackPlugin(['dist'], {
            root: `${myPaths.root}/../`,
            verbose: true,
            dry: false
        }),

        new StyleLintPlugin({
            // failOnError: true
        }),


        // Minifying CSS
        new MiniCssExtractPlugin({
            filename: '[name].[chunkhash].css',
            chunkFilename: '[id].css'
        }),

        // Creating manifest so we can get hashes for our output files
        new ManifestPlugin(),

        // Copy the images folder and optimize all the images
        new CopyWebpackPlugin([{
            from: `${myPaths.img}/**`,
            to: myPaths.dist,
            // ignore: ["favicon/**/*"]
        }, {
            from: `${myPaths.root}/vendor/**`,
            to: myPaths.dist
        }]),

        // new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i })

        // Creates a visualization of our bundle
        // new BundleAnalyzerPlugin()
    ]
});
