<?php
    $store_logos = array(
        'mobile'  => Utils\get_img_dir('svg/where-to-buy/mobile.svg'),
        'tablet'  => Utils\get_img_dir('svg/where-to-buy/tablet.svg'),
        'desktop' => Utils\get_img_dir('svg/where-to-buy/desktop.svg'),
    );
?>

<hr class="panel-content__separator landmark" style="margin: 0;">

<div class="band band--no-bottom">
    <h4 class="heading--delta heading--section">Find our products at</h4>
    <p class="vh">You can purchase this product at Amazon UK, Whole Foods Market, Planet Organic, Revital, and selected independent health stores.</p>

        <?php foreach($store_logos as $logo_key => $logo): ?>
            <div class="panel-content__store-logos panel-content__store-logos--<?= $logo_key ?>">
                <?= file_get_contents($logo); ?>
            </div>
        <?php endforeach; ?>
</div>