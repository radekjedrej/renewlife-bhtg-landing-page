<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PT7MWPF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="banner" role="banner">
    <a href="#main-content" class="skip-link">Skip to content</a>

    <div class="banner__inner">
        <a href="/" class="banner__link">
            <span class="vh">Home</span>

            <?= Utils\nb_load_template_part('partials/components/shared/renew-life-mark-logo', [
                'modifier' => 'banner__logo-mark'
            ]); ?>
    
            <?= file_get_contents( Utils\get_img_dir("svg/renew-life-text-logo.svg") ); ?>
        </a>
    </div>
</header>
