<section class="landmark--double">
    <div class="container ">
        <div class="responsive-embed landmark" class="js-plyr">
            <iframe src="https://player.vimeo.com/video/321728632" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen title="Renew Life Being Human Takes Guts Video" class="js-video-embed"></iframe>
        </div>
    </div>

    <div class="container container--reduced landmark text-center">
        <header>
            <h2 class="heading--bravo heading--section heading--primary">A healthy gut is a healthy you</h2>
        </header>
        <p>At Renew Life, our goal is to empower each and every one of our valued customers to make informed gut health decisions and improve their overall well-being through optimum digestive function and superior nutrition.</p>
        <p>As a company that focuses on digestive care and cleansing, we provide safe and effective natural solutions to digestive care issues and use only the highest-quality ingredients available.</p>
    </div>
</section>
